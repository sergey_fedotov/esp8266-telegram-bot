// Debugging switches and macros
#ifndef DEBUG_H_
#define DEBUG_H_

#ifndef DEBUG 
	#define DEBUG true // Switch debug output on and off by 1 or 0
#endif

#if DEBUG
#define PRINTS(s)   { Serial.print((s)); }
#define PRINTLN(s)   { Serial.println(s); }
#define PRINTF(s,v)  { Serial.printf( s, v); }
#define PRINT(s,v)  { Serial.print((s)); Serial.println(v); }

#define PRINTX(s,v) { Serial.print((s)); Serial.println(v, HEX);}
#define PRINTB(s,v)  { Serial.print((s)); Serial.println(v, BIN); }

#else
#define PRINTS(s)
#define PRINTLN(s)   
#define PRINTF(s,v) 
#define PRINT(s,v)
#define PRINTX(s,v) 
#define PRINTB(s,v) 
#endif

#endif

