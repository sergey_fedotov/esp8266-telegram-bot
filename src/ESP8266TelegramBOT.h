/*
  Copyright (c) 2021 Sergey Fedotov (fork of Giancarlo Bacchio). All right reserved.

  TelegramBot - Library to create your own Telegram Bot using 
  ESP8266 on Arduino IDE.
  Ref. Library at https://bitbucket.org/sergey_fedotov/esp8266-telegram-bot.git

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
  */


#ifndef ESP8266TelegramBOT_h
#define ESP8266TelegramBOT_h

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

//#include <WiFiClientSecure.h>
#define MAX_MESSAGES_IN_ONE_REQUEST 3
#define NUM_FIELDS 7


class TelegramBOT : public Printable
{
  public:
    TelegramBOT (void);
    TelegramBOT (String, String, String);
    
    size_t printTo (Print& p) const;
    
  	void begin(void);
  	void begin(String, String, String);
  	
  	void analizeMessages(void);
  	void sendMessage(String chat_id, String text, String reply_markup);
  	void sendMessage(int chat_id, String text, String reply_markup);
  	
//  	boolean getUpdates(String offset);
  	boolean getUpdates(int offset);
	//const char* fingerprint = "37:21:36:77:50:57:F3:C9:28:D0:F7:FA:4C:05:35:7F:60:C1:20:44";  //Telegram.org Certificate 
	struct FromT {
		int id;
		String firstName;
		String lastName;
		String userName;
	};
	int getMessagesCounter();
//	String getNextUpdate();
	int getUpdateId();
//	String getFromId();
	String* getMessage(int i);
	FromT getMessageFrom(int i);
	int getChatId(int i);
	String getMessageText(int i);
	String getUserName(int i);
	void setMessagesReaded();

	
  private:
    enum FieldNames { update_id, messages_counter=0, from_id, message_id=1, first_name, last_name, user_name, chat_id, text_body };
  	String message[MAX_MESSAGES_IN_ONE_REQUEST+1][NUM_FIELDS];  // amount of messages read per time  (update_id, from_id, name, lastname, chat_id, text)

    String connectToTelegram(String command);
    String _token;
    String _name;
    String _username;

    //WiFiClientSecure client;
    String urlEncode(String);
    String urlDecode(String);
    unsigned char h2int(char);
   
};



#endif
