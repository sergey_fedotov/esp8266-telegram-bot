
/*
  Copyright (c) 2021 Sergey Fedotov (fork of Giancarlo Bacchio). All right reserved.

  TelegramBot - Library to create your own Telegram Bot using 
  ESP8266 on Arduino IDE.
  Ref. Library at https://bitbucket.org/sergey_fedotov/esp8266-telegram-bot.git
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
  */


#include "ESP8266TelegramBOT.h"
//#include <WiFiClientSecureBearSSL.h>
#define DEBUG true
#include "debug.h"

TelegramBOT::TelegramBOT()	{
/*    _token="";
    _name="";
    _username=""; */
}

TelegramBOT::TelegramBOT(String token, String name, String username)	{
	_token=token;
	_name=name;
	_username=username;
}

void TelegramBOT::begin(String token, String name, String username)	{
	_token=token;
	_name=name;
	_username=username;
	begin();
}
void TelegramBOT::begin(void)	{
	  message[0][messages_counter]="0";   // number of received messages
	  message[1][update_id]="";
      message[0][from_id]="0";  // code of last read Message
}

//fake token for help
//1879820041:Ar94yAF4BCe1jJuG8G-R5PXFXF9O49FtFs4
//	10+     1+ 	18+			   1+ 	16  
String hideToken(String t){
	char *token = (char*)( t.c_str());
	for ( int i=11; i<29-2; i++ ){
		token[i] ='*';
	}
	for ( int i=29+1+2; i<29+1+16; i++ ){
		token[i] ='*';
	}
	return String(token);
}

size_t TelegramBOT::printTo (Print& p) const {
	size_t res = p.print("TOKEN = "); 
    res += p.println( hideToken(_token));
    res += p.print("NAME = " );
    res += p.println( _name);
    res += p.print("USERNAME = " );
    return res + p.println( _username);
    }

  
/**************************************************************************************************
 * function to achieve connection to api.telegram.org and send command to telegram                *
 * (Argument to pass: URL to address to Telegram)                                                 *
 **************************************************************************************************/
String TelegramBOT::connectToTelegram(String command)  {
    String mess="";
    long now;
//    bool avail;
    // Connect with api.telegram.org       
    IPAddress server(149,154,167,198);
    
    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);  client->setInsecure();
  HTTPClient https;
  
  
    /*client.setFingerprint("BB DC 45 2A 07 E3 4A 71 33 40 32 DA BE 81 F7 72 6F 4A 2B 6B");
    
    if (client.connect(server, 443)) {  
        //Serial.println(".... connected to server");
        String a="";
        char c;
	int ch_count=0;
        client.println("GET /"+command);
        now=millis();
        avail=false;
        while (millis()-now<1500) {  
            while (client.available()) {
	      char c = client.read();
              //Serial.write(c);
              if (ch_count<700)  {
                 mess=mess+c;
	         ch_count++;
	      }
              avail=true;
	    }
            if (avail) {
		//Serial.println();
		//Serial.println(mess);
		//Serial.println();
		break;
	    }
        }
    }*/
  
  /*client->connect("149.154.167.198", 443);
  if (!client->connected()) {
    Serial.printf("*** Can't connect. ***\n-------\n");
    return;
  }*/
  
  
 
  if (https.begin(*client, "https://api.telegram.org/"+command)) {  // HTTPS
    
    
  //Serial.println("Connected!\n-------\nhttps://api.telegram.org/");
 // Serial.println(command);
  //client->write("GET /"+command);
    
    //Serial.println("[HTTPS] GET...");
    int httpCode = https.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      //Serial.printf("[HTTPS] GET... code: %d\n", httpCode);
      // file found at server?
      if (httpCode == HTTP_CODE_OK) {
        String payload = https.getString();
        PRINTLN("[HTTPS] Received payload telegram: ");
        mess=payload;
        //Serial.println(String("1BTC = ") + payload + "USD");
      }
    } else {
        PRINTF("[HTTPS] GET telegram... failed, error: %s\n\r", https.errorToString(httpCode).c_str());
    }

    https.end();
  } else {
        PRINTLN("[HTTPS] Unable to connect BEAR::SSL telegram");
  }

    
    
    return mess;
}




/***************************************************************
 * GetUpdates - function to receive all messages from telegram *
 * (Argument to pass: the last+1 message to read)             *
 ***************************************************************/
//boolean TelegramBOT::getUpdates(int offset)  {
//	String command="bot"+_token+"/getUpdates?offset="+offset;
//	return getUpdates(String(offset));
//}
boolean TelegramBOT::getUpdates(int offset)  {
    
    PRINTLN("GET Update Messages ");
    String command="bot"+_token+"/getUpdates?offset="+offset;
    String mess=connectToTelegram(command);       //recieve reply from telegram.org
    // parsing of reply from Telegram into separate received messages
    int i=0;                //messages received counter
    if (mess!=""  && (WiFi.status() == WL_CONNECTED)) {
 //           Serial.print("Sent Update request messages up to : ");
 //           Serial.println(offset);
 //           Serial.print("RAW response: ");
 //          Serial.println(mess);
 //           Serial.println();
            String a="";
            int ch_count=0;
            String c;
            for (int n=1; n<mess.length()+1; n++) {   //Search for each message start
				yield();
				ch_count ++;
                c =  mess.substring(n-1,n);
                //Serial.print(c);
                a=a+c;
                if (ch_count>8) {
//		Serial.println(a);
                     if (a.substring(ch_count-9)=="update_id")  {
//            Serial.print("Request count = ");
//            Serial.println(i);
                         if (i>MAX_MESSAGES_IN_ONE_REQUEST) break;
						 message[i][messages_counter]=a.substring(0, ch_count-11);
                         a=a.substring(ch_count-11);
						 i ++;
                         ch_count=11;
                     }
                }
            }
	    if (i==1)  {
	        message[i][0]=a.substring(0, ch_count);   //Assign of parsed message into message matrix if only 1 message)
	    }
	    if (i>1)  i=i-1;
    }
    //check result of parsing process
    if (mess=="") {     
        PRINTLN("failed to update");
        return false;
    }   
    if (i==0) {
        PRINTLN("no new messages");
        PRINTLN();
        message[0][messages_counter]="0";
    }
    else {
        message[0][messages_counter]=String(i);   //returns how many messages are in the array
	//Serial.println();        	
	for (int b=1; b<i+1; b++)  {
          PRINTLN(message[b][update_id]);
        }
//        Serial.println();
        analizeMessages();
    }
    return true;
} 





/***********************************************************************
 * SendMessage - function to send message to telegram                  *
 * (Arguments to pass: chat_id, text_body to transmit and markup(optional)) *
 ***********************************************************************/
void TelegramBOT::sendMessage(int chat_id, String text, String reply_markup){
	sendMessage( String(chat_id), text, reply_markup);
}
void TelegramBOT::sendMessage(String chat_id, String text, String reply_markup)  {

    bool sent=false;
    PRINTLN("SEND Message: \""+ text+"\"");
    long sttime=millis();
    if ( text != ""  && (WiFi.status() == WL_CONNECTED)) {
	    while (millis()<sttime+8000) {    // loop for a while to send the message
			String command="bot"+_token+"/sendMessage?chat_id="+chat_id+"&text="+urlEncode(text)+"&reply_markup="+reply_markup;
			String mess=connectToTelegram(command);
			//Serial.println(mess);
			int messageLenght=mess.length();
			for (int m=5; m<messageLenght+1; m++)  {
				if (mess.substring(m-10,m)=="{\"ok\":true")     {  //Chek if message has been properly sent
					sent=true;
					break;
				}
			}
			if (sent==true)   {
				//  Serial.print("Message delivred: \"");
				//  Serial.print(text_body);
				//  Serial.println("\"");
				//  Serial.println();
				  break;
			}
			delay(1000);
		//	Serial.println("Retry");

	    }
    }
   // if (sent==false) Serial.println("Message not delivered");
}





/******************************************************************************
 * AnalizeMessage - function to achieve message parameters from json structure *
 ******************************************************************************/
void TelegramBOT::analizeMessages(void)     {

  int rif[NUM_FIELDS]= {0,0,0,0,0,0,0}; //pointers in message json (update_id, from_id, first_name, last_name, user_name, chat_id, text_body)
  int counter = message[0][messages_counter].toInt()+1;
  for (int i=1; i<counter; i++)      {
    int messageLenght=message[i][0].length();
    String a=message[i][update_id];
    message[i][text_body]="";
    for (int m=text_body; m<messageLenght+1; m++) {
		yield();
        if (a.substring(m-12,m)=="\"update_id\":")     { //Search for "update_id" pointer start
          rif[update_id]=m;
        }
        if (a.substring(m-13,m)=="\"from\":{\"id\":")  { //Search for "from" pointer start
          rif[from_id]=m;
        }
        if (a.substring(m-14,m)=="\"first_name\":\"")  { //Search for "first_name" pointer start
          rif[first_name]=m;
        }
        if (a.substring(m-13,m)=="\"last_name\":\"")   { //Search for "last_name" pointer start
          rif[last_name]=m;
        }
        if (a.substring(m-12,m)=="\"username\":\"")   { //Search for "username" pointer start
          rif[user_name]=m;
        }
        if (a.substring(m-13,m)=="\"chat\":{\"id\":")  { //Search for "chat" pointer start
          rif[chat_id]=m;
        }
        if (a.substring(m-8,m)=="\"text\":\"")         { //Search for "text_body" pointer start
	      rif[text_body]=m;
        }
        for (int n=0; n<2; n++)     {                    //Search for "update_id" and "from" pointers end
            if (a.substring(m-1,m)==",")  {
                if (rif[n]!=0)  {
                  message[i][n]=a.substring(rif[n],m-1);
                }
            rif[n]=0;
            }
        }
        if (a.substring(m-1,m)==",")  {                 //Search for "first name" pointer end
              if (rif[first_name]!=0)  {
                message[i][first_name]=a.substring(rif[first_name],m-2);  //Write value into dedicated slot
              }
          rif[first_name]=0;
        }
        if (a.substring(m-1,m)==",")  {                 //Search for "last_name" pointer end
              if (rif[last_name]!=0)  {
                message[i][last_name]=a.substring(rif[last_name],m-2);  //Write value into dedicated slot
              }
          rif[last_name]=0;
        }
        if (a.substring(m-1,m)==",")  {                 //Search for "user_name" pointer end
              if (rif[user_name]!=0)  {
                message[i][user_name]=a.substring(rif[user_name],m-2);  //Write value into dedicated slot
              }
          rif[user_name]=0;
        }
        if (a.substring(m-1,m)==",")  {                 //Search for "chat" pointer end
              if (rif[chat_id]!=0)  {
                message[i][chat_id]=a.substring(rif[chat_id],m-1);  //Write value into dedicated slot
              }
          rif[chat_id]=0;
        }
        if ((a.substring(m-2,m)=="\",") || (a.substring(m-2,m)=="\"}"))  {               //Search for "text" pointer end
              if (rif[text_body]!=0)  {
                message[i][text_body]=a.substring(rif[text_body],m-2);    //Write value into dedicated slot
            }
          rif[text_body]=0;
        }
        /*
	if (a.substring(m-2,m)=="\"}")  {               //Search for "text" pointer end
              if (rif[text_body]!=0)  {
                message[i][text_body]=a.substring(rif[text_body],m-2);    //Write value into dedicated slot
            }
          rif[text_body]=0;
        }  */
    }
    int lastMessageIndex = message[0][messages_counter].toInt();
    int lastId=message[lastMessageIndex][update_id].toInt();
    message[0][message_id]=lastId+1;                                   //Write id of last read message
//    PRINT("Next update Id: ", message[0][message_id] );
  //  for (int j=0; j<6; j++)	{
  //	Serial.println(message[i][j]);                                //print parsed data
  //  }
  }
 }
  
/*******************************************************************************
 *  getters funcs
 * *****************************************************************************/
  	int TelegramBOT::getMessagesCounter(){
		return message[0][messages_counter].toInt();
	}
/*	String TelegramBOT::getNextUpdate(){
		return message[0][from_id];
	}
	* */
	int TelegramBOT::getUpdateId(){
		return message[0][from_id].toInt();
	}
/*	String TelegramBOT::getMessageId(){
		 return message[0][message_id];
	}
*/
	TelegramBOT::FromT TelegramBOT::getMessageFrom(int i){
		TelegramBOT::FromT from;
		
		from.id = message[i][from_id].toInt();
		from.firstName = message[i][first_name];
		from.lastName = message[i][last_name];
		from.userName = message[i][user_name];
		//first_name, last_name, user_name
		return from;
	}
	String* TelegramBOT::getMessage(int i){
		return message[i];
	}
  	int TelegramBOT::getChatId(int i){
		return message[i][chat_id].toInt();
	}
	String TelegramBOT::getMessageText(int i){
		return message[i][text_body];
	}
	String TelegramBOT::getUserName(int i){
		return message[i][user_name];
	}
  
/*************** **********************
 * setter func 
 **************************************/
	void TelegramBOT::setMessagesReaded(){
		message[0][update_id] = "";
  }
  
  
 /******************************************************************************
 * URL Encoder - function to url encode/decode text_body *
 ******************************************************************************/
  String TelegramBOT::urlDecode(String str)
{
    
    String encodedString="";
    char c;
    char code0;
    char code1;
    for (int i =0; i < str.length(); i++){
        c=str.charAt(i);
      if (c == '+'){
        encodedString+=' ';  
      }else if (c == '%') {
        i++;
        code0=str.charAt(i);
        i++;
        code1=str.charAt(i);
        c = (h2int(code0) << 4) | h2int(code1);
        encodedString+=c;
      } else{
        
        encodedString+=c;  
      }
      
      yield();
    }
    
   return encodedString;
}

String TelegramBOT::urlEncode(String str)
{
    String encodedString="";
    char c;
    char code0;
    char code1;
    char code2;
    for (int i =0; i < str.length(); i++){
      c=str.charAt(i);
      if (c == ' '){
        encodedString+= '+';
      } else if (isalnum(c)){
        encodedString+=c;
      } else{
        code1=(c & 0xf)+'0';
        if ((c & 0xf) >9){
            code1=(c & 0xf) - 10 + 'A';
        }
        c=(c>>4)&0xf;
        code0=c+'0';
        if (c > 9){
            code0=c - 10 + 'A';
        }
        code2='\0';
        encodedString+='%';
        encodedString+=code0;
        encodedString+=code1;
        //encodedString+=code2;
      }
      yield();
    }
    return encodedString;
    
}

unsigned char TelegramBOT::h2int(char c)
{
    if (c >= '0' && c <='9'){
        return((unsigned char)c - '0');
    }
    if (c >= 'a' && c <='f'){
        return((unsigned char)c - 'a' + 10);
    }
    if (c >= 'A' && c <='F'){
        return((unsigned char)c - 'A' + 10);
    }
    return(0);
}

/*
String TelegramBOT::convertUnicode(String unicodeStr){
  String out = "";
  int len = unicodeStr.length();
  char iChar;
  char* error;
  for (int i = 0; i < len; i++){
     iChar = unicodeStr[i];
     if(iChar == '\\'){ // got escape char
       iChar = unicodeStr[++i];
       if(iChar == 'u'){ // got unicode hex
         char unicode[6];
         unicode[0] = '0';
         unicode[1] = 'x';
         for (int j = 0; j < 4; j++){
           iChar = unicodeStr[++i];
           unicode[j + 2] = iChar;
         }
         long unicodeVal = strtol(unicode, &error, 16); //convert the string
         out += (char)unicodeVal;
       } else if(iChar == '/'){
         out += iChar;
       } else if(iChar == 'n'){
         out += '\n';
       }
     } else {
       out += iChar;
     }
  }
  return out;
}
*/
