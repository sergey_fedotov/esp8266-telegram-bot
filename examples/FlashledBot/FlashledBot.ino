/*******************************************************************
 *  this is a basic example how to program a Telegram Bot          *
 *  using TelegramBOT library on ESP8266                           *
 *                                                                 *
 *  Open a conversation with the bot, you can command via Telegram *
 *  a led from ESP8266 GPIO                                        *
 *  https://web.telegram.org/#/im?p=@FlashledBot_bot               *
 *                                                                 *
 *  written by Giancarlo Bacchio                                   *
 *******************************************************************/

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <ESP8266TelegramBOT.h>


// Initialize Wifi connection to the router
char ssid[] = "xxxxxxxxxxxxxxxxxxxxxx";              // your network SSID (name)
char pass[] = "yyyyyyyy";                              // your network key



// Initialize Telegram BOT

#define BOTtoken "134745667:AAETzUWRQdb9xbMX_s-q_50U6ffgXcW3ldg"  //token of FlashledBOT
#define BOTname "FlashledBot"
#define BOTusername "FlashledBot_bot"

TelegramBOT bot; //(BOTtoken, BOTname, BOTusername);

int Bot_mtbs = 1000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done
bool Start = false;



/********************************************
 * EchoMessages - function to Echo messages *
 ********************************************/
void Bot_ExecMessages() {
  for (int i = 1; i < bot.message[0][bot.messages_counter].toInt() + 1; i++)      {
    bot.message[i][bot.text_body]=bot.message[i][bot.text_body].substring(1,bot.message[i][bot.text_body].length());
    if (bot.message[i][bot.text_body] == "\/ledon") {
      digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
      bot.sendMessage(bot.message[i][bot.from_id], "Led is ON", "");
    }
    if (bot.message[i][bot.text_body] == "\/ledoff") {
      digitalWrite(13, LOW);    // turn the LED off (LOW is the voltage level)
      bot.sendMessage(bot.message[i][bot.from_id], "Led is OFF", "");
    }
    if (bot.message[i][bot.text_body] == "\/start") {
      String wellcome = "Wellcome from FlashLedBot, your personal Bot on ESP8266 board";
      String wellcome1 = "/ledon : to switch the Led ON";
      String wellcome2 = "/ledoff : to switch the Led OFF";
      bot.sendMessage(bot.message[i][bot.from_id], wellcome, "");
      bot.sendMessage(bot.message[i][bot.from_id], wellcome1, "");
      bot.sendMessage(bot.message[i][bot.from_id], wellcome2, "");
      Start = true;
    }
  }
  bot.message[0][bot.messages_counter] = "";   // All messages have been replied - reset new messages
}


void setup() {
  Serial.begin(115200);
  delay(3000);

  // attempt to connect to Wifi network:
  Serial.print("Connecting Wifi: ");
  Serial.println(ssid);
  while (WiFi.begin(ssid, pass) != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  IPAddress ip = WiFi.localIP();
  Serial.println(ip);
  bot.begin(BOTtoken, BOTname, BOTusername);      // launch Bot functionalities
  pinMode(2, OUTPUT); // initialize digital pin 2 as an output.
}



void loop() {
  if (millis() > Bot_lasttime + Bot_mtbs)  {
    bot.getUpdates(bot.message[0][1]);   // launch API GetUpdates up to xxx message
    Bot_ExecMessages();   // reply to message with Echo
    Bot_lasttime = millis();
  }
}



